using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SCnML.Parser
{
	public class Component : Element
	{
		public String Type
		{
			get;
			set;
		}
		
		public String Text
		{
			get;
			set;
		}
		
		public IList<String> Attributes
		{
			get;
			private set;
		}
		
		public Component (String type, String text, IList<String> attributes = null)
		{
			Type = type;
			Text = text;
			Attributes = new List<String>(attributes);
		}

	    public override XElement GetXml()
	    {
	        var comp = new XElement("Component");
            if (Type == "ID")
            {
                comp.Add(new XElement("Id", Text));
            }
            else
            {
                comp.Add(new XElement("Type", Type));
                comp.Add(new XElement("Content", Text));
            }
	        foreach (var attr in Attributes)
	        {
	            comp.Add(new XElement("Attr", attr));
	        }
            foreach (var child in Childs)
            {
                comp.Add(child.GetXml());
            }
	        return comp;
	    }
	}
}

