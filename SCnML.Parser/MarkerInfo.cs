using System;

namespace SCnML.Parser
{
    /// <summary>
    /// Describes marker that can be occured into the parsing article.
    /// </summary>
    public abstract class MarkerInfo
    {
        protected MarkerInfo(String marker, MarkerType type)
        {
            Marker = marker;
            Type = type;
        }
		
        public String Marker
        {
            get;
            private set;
        }
		
        public MarkerType Type
        {
            get;
            private set;
        }
    }
}