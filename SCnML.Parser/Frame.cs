using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SCnML.Parser
{
	public class Frame : Element
	{
		public IList<String> Attributes
		{
			get;
			private set;
		}
		
		public Frame(IEnumerable<string> attributes)
		{
			Attributes = new List<String>(attributes);
		}

	    public override XElement GetXml()
        {
            var frame = new XElement("Frame");
            foreach (var attr in Attributes)
            {
                frame.Add(new XElement("Attr", attr));
            }
            foreach (var child in Childs)
            {
                frame.Add(child.GetXml());
            }
            return frame;
	    }
	}
}

