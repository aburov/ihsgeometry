using System;

namespace SCnML.Parser
{
    /// <summary>
    /// Incapsulates info about marker that means custom relation. Relation name must be specified
    /// in the article when marker of this type used.
    /// I.e. marker "=>" generally means custom binary forward relation.
    /// </summary>
    public class CustomMarkerInfo : MarkerInfo
    {
        public CustomMarkerInfo(String marker, MarkerType type)
            : base(marker, type)
        {
        }
    }
}