using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SCnML.Parser
{
	public class Set : Element
	{
		public IList<String> Attributes
		{
			get;
			private set;
		}
		
		public Set(IEnumerable<string> attributes)
		{
			Attributes = new List<String>(attributes);
		}

	    public override XElement GetXml()
	    {
            var set = new XElement("Set");
            foreach (var attr in Attributes)
            {
                set.Add(new XElement("Attr", attr));
            }
            foreach (var child in Childs)
            {
                set.Add(child.GetXml());
            }
            return set;
	    }
	}
}

