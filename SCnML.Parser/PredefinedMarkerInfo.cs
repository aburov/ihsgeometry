using System;

namespace SCnML.Parser
{
    /// <summary>
    /// Incapsulates info about marker that means concrete predefined relation.
    /// I.e. marker "=" generally means "synonym*" relation. It can be specified using this class.
    /// </summary>
    public class PredefinedMarkerInfo : MarkerInfo
    {
        public PredefinedMarkerInfo(String marker, MarkerType type, String relation)
            : base(marker, type)
        {
            Relation = relation;
        }
		
        public String Relation
        {
            get;
            private set;
        }
    }
}