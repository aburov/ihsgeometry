using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

namespace SCnML.Parser
{
	public abstract class Element
	{
		public Element()
		{
			Childs = new List<Element>();
		}
		
		public IList<Element> Childs
		{
			get;
			private set;
		}

	    public abstract XElement GetXml();
	}
}

