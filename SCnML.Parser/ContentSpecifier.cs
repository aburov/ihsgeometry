using System;

namespace SCnML.Parser
{
    /// <summary>
    /// Content specifier: describes content limits and it's type.
    /// </summary>
    public class ContentSpecifier
    {
        public ContentSpecifier(String startsWith, String endsWith, String contentType)
        {
            StartsWith = startsWith;
            EndsWith = endsWith;
            ContentType = contentType;
        }
		
        public String StartsWith
        {
            get;
            private set;
        }
		
        public String EndsWith
        {
            get;
            private set;
        }
		
        public String ContentType
        {
            get;
            private set;
        }
    }
}