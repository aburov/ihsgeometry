using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace SCnML.Parser
{
    /// <summary>
	/// The SCnML parser implementation.
	/// </summary>
	public class Parser
	{
		private readonly IList<MarkerInfo> markers;
		private readonly IList<ContentSpecifier> contentLimits;
		private static readonly Regex LineEndRegEx = new Regex(@"\r\n", RegexOptions.Compiled);
		private static readonly Regex BlankLineRegEx = new Regex(@"^\s*\r\n$",
		                                              RegexOptions.IgnoreCase |
		                                              RegexOptions.Multiline |
		                                              RegexOptions.Compiled);
		
		private String source;
		private String originalSource;
		
		// Counters
		private int position;
		private int curlyBrackets;
        private int originalLength;
		
		public Parser(IList<MarkerInfo> markers, IList<ContentSpecifier> contentLimits)
		{
			this.markers = markers;
			this.contentLimits = contentLimits;
		}
		
		public Article Parse(String scnmlSource)
		{
			source = scnmlSource;
		    PrepareSource();
			InitParser();
			return ParseArticle();
		}
		
		private void PrepareSource()
		{
			RemoveComments();
			UnifyLineEnds();
			RemoteBlankLines();
		}
		
		private void UnifyLineEnds()
		{
			source = LineEndRegEx.Replace(source, "\n");
		}
		
		private void RemoteBlankLines()
		{
            source = BlankLineRegEx.Replace(source, "");
		}
		
		private void RemoveComments()
		{
			var newSource = "";
			var inComment = false;
			var hasBackSlash = false;
			var i = 0;
			var len = source.Length - 1;
			while(i < len)
			{
				var c1 = source[i];
				var c2 = source[i+1];
				if (inComment)
				{
					if (c1 == '*' && c2 == '/' && !hasBackSlash)
					{
						inComment = false;
						i++;
					}
				}
				else 
				{
					if (c1 == '/' && c2 == '*' && !hasBackSlash)
					{
						inComment = true;
						i++;
					}
					else
					{
						newSource += c1;
					}
				}
				hasBackSlash = !hasBackSlash && c1 == '\\';
				i++;
			}
			if (!inComment)
			{
				newSource += source[len];
			}
		    source = newSource;
		}
		
		private void InitParser()
		{
			originalSource = source;
			position = curlyBrackets = 0;
		    originalLength = originalSource.Length;
		}
		
		private Article ParseArticle()
		{
			var indent = ParseIndent();
			var concept = ParseId();
			if (concept == null)
			{
				return null;
			}
			var article = new Article(concept);
			ParseComponentInner(article, indent + 1);
			return article;
		}
		
		private String ParseId()
		{
			var id = String.Empty;
			var hasUnderscore = false;
			foreach (var ch in source)
			{
				if (ch != '\n' && (ch != ':' || !hasUnderscore))
				{
					hasUnderscore = ch == '_' || ((ch == ' ' || ch == '\t') && hasUnderscore);
					id += ch;
				}
				else
				{
					MoveNextChar(id.Length);
					return id;
				}
			}
			return null;
		}
		
		private void ParseComponentInner(Element parent, int indent)
		{
			ParseNewLine();
			while (GetIndent() == indent)
			{
				ParseIndent();
				var marker = ParseMarker();
				if (marker != null)
				{
					if (marker is PredefinedMarkerInfo)
					{
						var conn = new Connective(marker.Type, ((PredefinedMarkerInfo)marker).Relation);
						parent.Childs.Add (conn);
						ParseInlineComponent(conn, indent);
					}
					else
					{
						ParseSpace();
						var relation = ParseId();
						var conn = new Connective(marker.Type, relation);
						parent.Childs.Add(conn);
						ParseComponents(conn, indent + 1);
					}
				}
				else
				{
					throw new Exception(String.Format("Expected marker at line {0}.", GetCurrentLine())); 
				}
			}
		}
		
		private MarkerInfo ParseMarker()
		{
		    var marker = markers.FirstOrDefault(m => IsMatchMarker(m.Marker));
            if (marker != null)
		    {
                MoveNextChar(marker.Marker.Length);
		    }
		    return marker;
		}

        private bool IsContentFollows()
		{
		    return contentLimits.Any(c => source.StartsWith(c.StartsWith));
		}

        private void ParseComponents(Element parent, int indent)
		{
			ParseNewLine();
			while (GetIndent() == indent)
			{
				ParseIndent();
				if (source[0] == '}')
				{
				    if (curlyBrackets > 0)
					{
						curlyBrackets--;
						MoveNextChar();
						return;
					}
				    throw new Exception(String.Format(@"Unexpected '}}' at line {0}.", GetCurrentLine()));
				}
			    ParseInlineComponent(parent, indent);
				ParseNewLine();
			}
		}
		
		private void ParseInlineComponent(Element parent, int indent)
		{
			var attrs = new List<String>();
			while (IsAttrFollows())
			{
				var id = ParseId();
				MoveNextChar();
				ParseSpace();
				attrs.Add(id);
			}
			ParseSpace();
			Element comp;
			if (IsContentFollows())
			{
				var content = ParseContent();
				if (content != null)
				{
					comp = new Component(content.ContentType, content.Content, attrs);
					parent.Childs.Add(comp);
				}
				else
				{
					throw new Exception(String.Format("Content expected at line {0}.", GetCurrentLine()));
				}
			}
			else if (source[0] == '{')
			{
				curlyBrackets++;
				MoveNextChar();
				comp = new Set(attrs);
				parent.Childs.Add(comp);
				ParseComponents(comp, indent + 1);
			}
			else
			{
				var id = ParseId();
				comp = new Component("ID", id, attrs);
				parent.Childs.Add(comp);
			}
			ParseComponentInner(comp, indent + 1);
		}
		
		private bool IsAttrFollows()
		{
			var attr = String.Empty;
			var hasUndescore = false;
			foreach (var ch in source)
			{
				if (ch != '\n' && (ch != ':' || !hasUndescore))
				{
					hasUndescore = ch == '_' || ((ch == ' ' || ch == '\t') && hasUndescore);
					attr += ch;
				}
				else if (ch == ':')
				{
					return attr.Length > 0;
				}
				else
				{
					return false;
				}	
			}
			return false;
		}
		
		private ParsedContent ParseContent()
		{
			foreach (var c in contentLimits)
			{
				if (source.StartsWith(c.StartsWith))
				{
					MoveNextChar(c.StartsWith.Length);
					return new ParsedContent(c.ContentType, ReadUntil(c.EndsWith));
				}
			}
			return null;
		}
		
		private String ReadUntil(String factor, String current = "", bool hasBackSlash = false)
		{
			current = current ?? String.Empty;
			if (source.StartsWith(factor) && !hasBackSlash)
			{
				MoveNextChar(factor.Length);
				return current;
			}
		    var ch = source[0];
		    hasBackSlash = !hasBackSlash && ch == '\\';
		    MoveNextChar();
		    if (!hasBackSlash)
		    {
		        current += ch;
		    }
		    return ReadUntil(factor, current, hasBackSlash);
		}
		
		private bool IsMatchMarker(String marker)
		{
			var nextChar = source[marker.Length];
			return source.StartsWith(marker) && (nextChar == ' ' || nextChar == '\t');
		}
		
		private void ParseNewLine()
		{
			while (position < originalLength && source[0] == '\n')
			{
				MoveNextChar();
			}
		}
		
		private void ParseSpace()
		{
            while (position < originalLength && (source[0] == ' ' || source[0] == '\t'))
			{
				MoveNextChar();
			}
		}
		
		private int ParseIndent(int current = 0)
		{
			var ch = source[0];
			if (ch == ' ')
			{
				current++;
			}
			else if (ch == '\t')
			{
				current += 4 - (current + 4) % 4;
			}
			else
			{
				return (current - current % 4) / 4;
			}
			MoveNextChar();
			return ParseIndent(current);
		}
		
		private int GetIndent(int current = 0, int offset = 0)
		{
            if (originalLength == position) return 0;
			var ch = source[offset];
			if (ch == ' ')
			{
				current++;
			}
			else if (ch == '\t')
			{
				current += 4 - (current + 4) % 4;
			}
			else
			{
				return (current - current % 4) / 4;
			}
			offset++;
			return GetIndent(current, offset);
		}
		
		private void MoveNextChar(int count = 1)
		{
			position += count;
			source = source.Substring(count);
		}
		
		private int GetCurrentLine()
		{
			var count = 0;
			var i = 0;
			while (i < position)
			{
				if (originalSource[i++] == '\n')
				{
					count ++;
				}
			}
			return count + 1;
		}
		
		protected class ParsedContent
		{
			public ParsedContent(String contentType, String content)
			{
				ContentType = contentType;
				Content = content;
			}
			
			public String ContentType
			{
				get;
				private set;
			}
			
			public String Content
			{
				get;
				private set;
			}
		}
	}
}

