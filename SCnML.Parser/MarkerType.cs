namespace SCnML.Parser
{
    /// <summary>
    /// Marker type describes count of components of describing relation and direction of binary
    /// relations relative to concept (or parent component).
    /// </summary>
    public enum MarkerType
    {
        BinForward = 0,
        BinBackward = 1,
        BinNone = 2,
        Custom = 3
    }
}