using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SCnML.Parser
{
	public class Connective : Element
	{
		public MarkerType MarkerType
		{
			get;
			set;
		}
		
		public String Relation
		{
			get;
			set;
		}
		
		public IList<String> Attributes
		{
			get;
			private set;
		}
		
		public Connective(MarkerType type, String relation, IList<String> attributes = null)
		{
			MarkerType = type;
			Relation = relation;
			Attributes = new List<String>(attributes ?? new List<String>());
		}

	    public override XElement GetXml()
        {
            var set = new XElement("Connective",
                new XElement("Type", GetConnectiveType()),
                new XElement("Relation", Relation));
            foreach (var attr in Attributes)
            {
                set.Add(new XElement("Attr", attr));
            }
            foreach (var child in Childs)
            {
                set.Add(child.GetXml());
            }
            return set;
	    }

        protected virtual String GetConnectiveType()
        {
            return MarkerType.ToString();
        }
	}
}

