using System;
using System.Xml.Linq;

namespace SCnML.Parser
{	
	public class Article : Element
	{
		public String Concept
		{
			get;
			set;
		}
		
		public Article (String concept)
		{
			Concept = concept;
		}

	    public override XElement GetXml()
	    {
            var root = new XElement("SCnArticle", new XElement("Concept", Concept));
            foreach (var child in Childs)
            {
                root.Add(child.GetXml());
            }
	        return root;
	    }
	}
}

