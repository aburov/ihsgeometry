using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SCnML.Parser;

namespace IHSGeometry.Test
{	
	class MainClass
	{
		public static void Main (string[] args)
		{
			var markers = new List<MarkerInfo>
			    {
					new PredefinedMarkerInfo("=", MarkerType.BinForward, "synonym"),
					new PredefinedMarkerInfo("->", MarkerType.BinForward, "contains"),
					new PredefinedMarkerInfo("<-", MarkerType.BinBackward, "contains"),
					new PredefinedMarkerInfo("~>", MarkerType.BinForward, "varContains"),
					new PredefinedMarkerInfo("<~", MarkerType.BinBackward, "varContains"),
					new CustomMarkerInfo("=>", MarkerType.BinForward),
					new CustomMarkerInfo("<=", MarkerType.BinBackward),
					new CustomMarkerInfo("<=>", MarkerType.BinNone),
					new CustomMarkerInfo("$", MarkerType.Custom)
				};
			var contents = new List<ContentSpecifier>
			    {
					new ContentSpecifier("[\"", "\"]", "Content@1"),
					new ContentSpecifier("{\"", "\"}", "Content@2")
				};
			var parser = new Parser(markers, contents);
            var article = parser.Parse(File.ReadAllText("gf.scnml"));
            var xml = new XDocument(article.GetXml());
            xml.Save("gf.xml");
            Console.WriteLine(article.Childs.OfType<Connective>()
                .Single(c => c.Relation == "ξοπεδελενθε*")
                .Childs.Single()
                .Childs.OfType<Connective>().Single(c => c.Relation == "synonym")
                .Childs.OfType<Component>().Single().Text);
            Console.ReadKey();
		}
	}
}
