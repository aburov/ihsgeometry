using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Gdk;
using Gtk;
using Mono.TextEditor;
using SCnML.Parser;
using Window = Gtk.Window;
using WindowType = Gtk.WindowType;

public partial class MainWindow : Window
{
    private String baseTitle = "SCn Editor";
	private readonly TextEditor textEditor = new TextEditor();
    private readonly TreeStore filesListStore = new TreeStore(typeof(string), typeof(string), typeof(string));
    private readonly Parser parser;

	public MainWindow()
		: base(WindowType.Toplevel)
	{
        var markers = new List<MarkerInfo>
			    {
					new PredefinedMarkerInfo("=", MarkerType.BinForward, "synonym"),
					new PredefinedMarkerInfo("->", MarkerType.BinForward, "contains"),
					new PredefinedMarkerInfo("<-", MarkerType.BinBackward, "contains"),
					new PredefinedMarkerInfo("~>", MarkerType.BinForward, "varContains"),
					new PredefinedMarkerInfo("<~", MarkerType.BinBackward, "varContains"),
					new CustomMarkerInfo("=>", MarkerType.BinForward),
					new CustomMarkerInfo("<=", MarkerType.BinBackward),
					new CustomMarkerInfo("<=>", MarkerType.BinNone),
					new CustomMarkerInfo("$", MarkerType.Custom)
				};
        var contents = new List<ContentSpecifier>
			    {
					new ContentSpecifier("[\"", "\"]", "Content@1"),
					new ContentSpecifier("{\"", "\"}", "Content@2")
				};
        parser = new Parser(markers, contents);
		Build();
	    SetUpFilesTreeView();
		textEditorScrolledWindow.Child = textEditor;
	    Title = "SCn Editor";
	    LoadFiles();
	}
	
	protected void OnDeleteEvent(object sender, DeleteEventArgs a)
	{
		Application.Quit();
		a.RetVal = true;
	}

    protected void LoadFiles()
    {
        var directory = AppDomain.CurrentDomain.BaseDirectory + "../../../source/";
        var iter = filesListStore.AppendValues("Concepts");
        var map = new Dictionary<String, List<String>>();
        var levels = new Dictionary<String, int>();
        foreach (var file in Directory.GetFiles(directory).Where(f => f.EndsWith(".scnml")).Select(f => new FileInfo(f)))
        {
            var article = parser.Parse(File.ReadAllText(file.FullName));
            if (article != null)
            {
                var subIter = filesListStore.AppendValues(iter, article.Concept, 0, file.FullName);
                var definition = article.Childs.OfType<Connective>()
                    .SingleOrDefault(c => c.Relation == "�����������*");
                var consts = new List<String>();
                if (definition != null)
                {
                    var constConn = definition.Childs.Single().Childs.OfType<Connective>()
                        .SingleOrDefault(c => c.Relation == "������������ ���������*");
                    if (constConn != null)
                    {
                        foreach (var id in constConn.Childs.OfType<Component>().Select(c => c.Text))
                        {
                            filesListStore.AppendValues(subIter, id, "", "");
                            consts.Add(id);
                        }
                    }
                }
                map.Add(article.Concept, consts);
            }
        }
        var level = 0;
        var xs = new Dictionary<int, int>();
        while (true)
        {
            var determinedLevels = 0;
            foreach (var concept in map.Keys.Where(c => !levels.Keys.Contains(c)))
            {
                if (map[concept].All(c => levels.Keys.Contains(c)))
                {
                    levels[concept] = level;
                    determinedLevels++;
                }
            }
            if (determinedLevels == 0)
            {
                break;
            }
            xs[level] = 0;
            level++;
        }
        level += 2;
        var ssc = new XElement("staticSector");
        var sid = 38880500;
        var ids = new Dictionary<String, String>();
        var random = new Random();
        var points = new Dictionary<String, Point>();
        foreach (var node in map.Keys)
        {
            var id = (sid++).ToString(CultureInfo.InvariantCulture);
            var x = levels.Keys.Contains(node) ? (xs[levels[node]]+=120) : random.Next(0, 300);
            var y = levels.Keys.Contains(node) ? levels[node] * 120 : random.Next(level * 120, level * 120 + 300);
            ids.Add(node, id);
            points.Add(node, new Point(x, y));
            ssc.Add(new XElement("node",
                new XAttribute("type", "node/const/general_node"),
                new XAttribute("idtf", node),
                new XAttribute("shapeColor", "0"),
                new XAttribute("parent", 0),
                new XAttribute("id", id),
                new XAttribute("x", x),
                new XAttribute("y", y),
                new XAttribute("haveBus", "false"),
                new XElement("content", new XAttribute("type", "0"), new XAttribute("mime_type", ""), new XAttribute("file_name", ""))));
        }
        foreach (var b in map.Keys)
        {
            //<arc type="arc/const/pos" idtf="" shapeColor="0" id="7258512" parent="0" id_b="38880352" id_e="38880496" b_x="-105" b_y="75" e_x="15" e_y="75" dotBBalance="0" dotEBalance="0">
            //    <points/>
            //</arc>
            foreach (var e in map[b])
            {
                var id = (sid++).ToString(CultureInfo.InvariantCulture);
                ssc.Add(new XElement("arc",
                    new XAttribute("type", "arc/const/pos"),
                    new XAttribute("idtf", ""),
                    new XAttribute("shapeColor", "0"),
                    new XAttribute("id", id),
                    new XAttribute("parent", "0"),
                    new XAttribute("id_b", ids[b]),
                    new XAttribute("id_e", ids[e]),
                    new XAttribute("b_x", points[b].X),
                    new XAttribute("b_y", points[b].Y),
                    new XAttribute("e_x", points[e].X),
                    new XAttribute("e_y", points[e].Y),
                    new XAttribute("dotBBalance", "0"),
                    new XAttribute("dotEBalance", "0"),
                    new XElement("points")));
            }
        }
        var doc = new XDocument(new XDeclaration("1.0", "windows-1251", "yes"), new XElement("GWF", new XAttribute("version", "2.0"), ssc));
        doc.Save(AppDomain.CurrentDomain.BaseDirectory + "../../../source/struct.gwf");
    }

    protected void SetUpFilesTreeView()
    {
        filesTreeView.AppendColumn(new TreeViewColumn { Title = "Concept", MinWidth = 100, Resizable = true });
        filesTreeView.AppendColumn(new TreeViewColumn { Title = "Lvl", MaxWidth = 50 });
        filesTreeView.Model = filesListStore;
        var cellRenderer = new CellRendererText();
        filesTreeView.Columns[0].PackStart(cellRenderer, true);
        filesTreeView.Columns[0].AddAttribute(cellRenderer, "text", 0);
        cellRenderer = new CellRendererText();
        filesTreeView.Columns[1].PackStart(cellRenderer, true);
        filesTreeView.Columns[1].AddAttribute(cellRenderer, "text", 1);
        filesTreeView.ButtonPressEvent += OnTreeViewButtonPressEvent;
    }

    [GLib.ConnectBefore]
    protected void OnTreeViewButtonPressEvent(object sender, ButtonPressEventArgs e)
    {
        if (e.Event.Type == EventType.TwoButtonPress)
        {
            TreeModel model;
            TreeIter iter;
            if (filesTreeView.Selection.GetSelected(out model, out iter))
            {
                if (model.GetPath(iter).Depth == 2)
                {
                    Title = baseTitle + " � " + model.GetValue(iter, 2);
                    textEditor.Text = File.ReadAllText(model.GetValue(iter, 2).ToString());
                }
                else if (model.IterHasChild(iter))
                {
                    if (filesTreeView.GetRowExpanded(model.GetPath(iter)))
                    {
                        filesTreeView.CollapseRow(model.GetPath(iter));
                    }
                    else
                    {
                        filesTreeView.ExpandRow(model.GetPath(iter), false);
                    }
                }
            }
        }
    }
}
